# FFTW test on C++

Tests, useful info, pieces of code relating to the use and configuration of `fftw`.

## Documentation

Can be found [from the fftw.org website](https://fftw.org/#documentation).

Simple tutorial [on basic usage of FFTW](https://fftw.org/fftw2_doc/fftw_2.html)

## Related reading

[Computing the discrete fourier transform of audio data with FFTW](https://stackoverflow.com/questions/39839391/computing-the-discrete-fourier-transform-of-audio-data-with-fftw)

## Installing

```bash
# On Ubuntu
sudo apt update
sudo apt install libfftw3-3 libfftw3-dev libfftw3-doc
```

## CMake inclusion

```cmake
# Simple CMakeLists.txt file template for including FFTW to a C++ project
project(fftw-tests)
cmake_minimum_required(VERSION 3.22)

# Set this to a standard of your liking
set(CMAKE_CXX_STANDARD 14)

find_package(PkgConfig REQUIRED)
pkg_search_module(FFTW3 REQUIRED fftw3f)

add_executable(fftw-tests main.cpp)

target_include_directories(
    fftw-tests PUBLIC
    ${FFTW3F_INCLUDE_DIRS}
)

target_link_libraries(
    fftw-tests PUBLIC
    ${FFTW3F_LIBRARIES}
)
```